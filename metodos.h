#ifndef METODOS_H
#define METODOS_H

void metodoExplicito(double a, int m, int n, double dt, double tFinal);

void metodoImplicito(double a, int m, int n, double dt, double tFinal);

void metodoCrankNicolson(double a, int m, int n, double dt, double tFinal);

void printVetorComoMatriz(double* v, int m, int n);

void solExataEquacaoDoCalor(double* u, int m, int n, double t);

double calculaErroSolEquacaoDoCalor(double* u, int m, int n, double t);

double contruirMatrizA(int m, int n, double D, double B, double A, double C, double E);

#endif
