\documentclass[
	% -- opções da classe memoir --
	12pt,				% tamanho da fonte
	openright,			% capítulos começam em pág ímpar (insere página vazia caso preciso)
	oneside,			% para impressão em recto e verso. Oposto a oneside
	a4paper,			% tamanho do papel. 
	% -- opções da classe abntex2 --
	%chapter=TITLE,		% títulos de capítulos convertidos em letras maiúsculas
	%section=TITLE,		% títulos de seções convertidos em letras maiúsculas
	%subsection=TITLE,	% títulos de subseções convertidos em letras maiúsculas
	%subsubsection=TITLE,% títulos de subsubseções convertidos em letras maiúsculas
	% -- opções do pacote babel --
	english,			% idioma adicional para hifenização
	brazil				% o último idioma é o principal do documento
	]{abntex2}

% ---
% Pacotes básicos 
% ---
\usepackage{lmodern}			% Usa a fonte Latin Modern			
\usepackage[T1]{fontenc}		% Selecao de codigos de fonte.
\usepackage[utf8]{inputenc}		% Codificacao do documento (conversão automática dos acentos)
\usepackage{lastpage}			% Usado pela Ficha catalográfica
\usepackage{indentfirst}		% Indenta o primeiro parágrafo de cada seção.
\usepackage{color}				% Controle das cores
\usepackage{graphicx}			% Inclusão de gráficos
\graphicspath{ {images/} }
\usepackage{microtype} 			% para melhorias de justificação
% ---
		

% ---
% Pacotes adicionais, usados apenas no âmbito do Modelo Canônico do abnteX2
% ---
\usepackage{lipsum}				% para geração de dummy text
% ---

% ---
% Pacotes de citações
% ---
\usepackage[brazilian,hyperpageref]{backref}	 % Paginas com as citações na bibl
\usepackage[alf]{abntex2cite}	% Citações padrão ABNT

% --- 
% CONFIGURAÇÕES DE PACOTES
% --- 

% ---
% Configurações do pacote backref
% Usado sem a opção hyperpageref de backref
\renewcommand{\backrefpagesname}{Citado na(s) página(s):~}
% Texto padrão antes do número das páginas
\renewcommand{\backref}{}
% Define os textos da citação
\renewcommand*{\backrefalt}[4]{
	\ifcase #1 %
		Nenhuma citação no texto.%
	\or
		Citado na página #2.%
	\else
		Citado #1 vezes nas páginas #2.%
	\fi}%
% ---

% ---
% Informações de dados para CAPA e FOLHA DE ROSTO
% ---
\titulo{3\textsuperscript{\underline{o}} Trabalho de Algoritmos Numéricos II}
\autor{Eduardo Motta de Oliveira}
\local{Brasil}
\data{2017}


% ---
% Configurações de aparência do PDF final

% alterando o aspecto da cor azul
\definecolor{blue}{RGB}{41,5,195}

% informações do PDF
\makeatletter
\hypersetup{
     	%pagebackref=true,
		pdftitle={\@title}, 
		pdfauthor={\@author},
    	pdfsubject={\imprimirpreambulo},
	    pdfcreator={LaTeX with abnTeX2},
		pdfkeywords={abnt}{latex}{abntex}{abntex2}{trabalho acadêmico}, 
		colorlinks=true,       		% false: boxed links; true: colored links
    	linkcolor=blue,          	% color of internal links
    	citecolor=blue,        		% color of links to bibliography
    	filecolor=magenta,      		% color of file links
		urlcolor=blue,
		bookmarksdepth=4
}
\makeatother
% --- 

% --- 
% Espaçamentos entre linhas e parágrafos 
% --- 

% O tamanho do parágrafo é dado por:
\setlength{\parindent}{1.3cm}

% Controle do espaçamento entre um parágrafo e outro:
\setlength{\parskip}{0.2cm}  % tente também \onelineskip

% ---
% compila o indice
% ---
\makeindex
% ---

% ----
% Início do documento
% ----
\begin{document}

% Seleciona o idioma do documento (conforme pacotes do babel)
%\selectlanguage{english}
\selectlanguage{brazil}


% ----------------------------------------------------------
% ELEMENTOS PRÉ-TEXTUAIS
% ----------------------------------------------------------
% \pretextual

% ---
% Capa
% ---
\imprimircapa


% ---
% inserir o sumario
% ---
%\pdfbookmark[0]{\contentsname}{toc}
%\tableofcontents*
%\cleardoublepage
% ---



% ----------------------------------------------------------
% ELEMENTOS TEXTUAIS
% ----------------------------------------------------------
\textual

% ----------------------------------------------------------
% Introdução (exemplo de capítulo sem numeração, mas presente no Sumário)
% ----------------------------------------------------------
\chapter*[Introdução]{Introdução}
Este trabalho tem por objetivo realizar um estudo do uso dos métodos de diferenças finitas aplicado à equação de Poisson bidimensional transiente.

O trabalho se dá em três etapas, onde são consideradas diferentes situações para a aplicação dos métodos de aproximação. Os esquemas explícito, implícito e crank-nicolson são implementados juntamente com as soluções de contorno dadas. A solução dos sistemas lineares que aparecem durante a execução dos métodos será efetuada utilizando o método GMRES com precondicionador ILU, implementado no trabalho anterior.

Resultados serão apresentados e comparados em cada seção e as conclusões serão discutidas ao final.


\chapter{Referencial Teórico}

Em busca de maior eficiência na execução dos métodos numéricos de resolução de sistemas lineares algumas técnicas foram criadas. Entre elas, podemos destacar o precondicionamento e o reordenamento. 

O precondicionamento e reordenamento alteram a estrutura do sistema sem alterar a solução final de forma que a convergência ocorra mais rapidamente ou que de fato se consiga a convergência em sistemas de difícil solução por métodos tradicionais. 



\section{Método das Diferenças Finitas}


$\frac{\partial u}{\partial t} - a \Big( \frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u }{\partial y^2} \Big) = f(x,y,t) $

\subsection{Diferença Adiantada}

Reescrevendo usando diferença adiantada:

$\frac{u^{k+1}_I - u^k_I}{\Delta t} 
- a \Big( \frac{u^k_{I-1} - 2 u^k_I + u^k_{I+1} }{h^2_x} 
+ \frac{u^k_{I-n} - 2 u^k_I + u^k_{I+n}}{h_y^2} \Big) = f^k_I $

Reordenando:
\begin{center}
$u_I^{k+1} = 
\Big( a  \frac{\Delta t}{h_y^2} \Big) u_{I-n}^k 
+ \Big( a  \frac{\Delta t}{h_x^2} \Big) u_{I-1}^k 
+ \Big[ 1-2a\Delta t\Big( \frac{1}{h_x^2} + \frac{1}{h_y^2}  \Big) \Big] u_{I}^k 
+ \Big( a  \frac{\Delta t}{h_x^2} \Big) u_{I+1}^k 
+ \Big( a  \frac{\Delta t}{h_y^2} \Big) u_{I+n}^k
+ \Delta t f_I^k $
\end{center}

Renomeando:

$\tilde{d}_I^k =  a  \frac{\Delta t}{h_y^2}$

$\tilde{b}_I^k =  a  \frac{\Delta t}{h_x^2}$

$\tilde{a}_I^k = 1 -2a\Delta t\Big( \frac{1}{h_x^2} + \frac{1}{h_y^2}  \Big)$

$\tilde{c}_I^k =  a  \frac{\Delta t}{h_x^2}$

$\tilde{e}_I^k =  a  \frac{\Delta t}{h_y^2}$

Então:

$u_I^{k+1} = 
\tilde{d}_I^k u_{I-n}^k 
+ \tilde{b}_I^k u_{I-1}^k 
+ \tilde{a}_I^k u_{I}^k 
+ \tilde{c}_I^k u_{I+1}^k 
+ \tilde{e}_I^k u_{I+n}^k
+ \Delta t f_I^k $

O que nos conduz a equação:

$U^{k+1} = B U^k + \Delta t f^k$

\newpage
\subsection{Diferença Atrasada}

Reescrevendo usando diferença atrasada:

$\frac{u^{k+1}_I - u^k_I}{\Delta t} 
- a \Big( \frac{u^{k+1}_{I-1} - 2 u^{k+1}_I + u^{k+1}_{I+1} }{h^2_x} 
+ \frac{u^{k+1}_{I-n} - 2 u^{k+1}_I + u^{k+1}_{I+n}}{h_y^2} \Big) = f^{k+1}_I $

Reordenando:

\begin{center}
$
\Big( -a  \frac{\Delta t}{h_y^2} \Big) u_{I-n}^{k+1} 
+ \Big( -a  \frac{\Delta t}{h_x^2} \Big) u_{I-1}^{k+1} 
+ \Big[ 1+2a\Delta t\Big( \frac{1}{h_x^2} + \frac{1}{h_y^2}  \Big) \Big] u_{I}^{k+1} 
+ \Big( -a  \frac{\Delta t}{h_x^2} \Big) u_{I+1}^{k+1} 
+ \Big( -a  \frac{\Delta t}{h_y^2} \Big) u_{I+n}^{k+1}$

 $= u_I^{k} + \Delta t f_I^{k+1} $
\end{center}

Renomeando:

$\tilde{d}_I^k =  -a  \frac{\Delta t}{h_y^2}$

$\tilde{b}_I^k = - a  \frac{\Delta t}{h_x^2}$

$\tilde{a}_I^k = 1 +2a\Delta t\Big( \frac{1}{h_x^2} + \frac{1}{h_y^2}  \Big)$

$\tilde{c}_I^k = - a  \frac{\Delta t}{h_x^2}$

$\tilde{e}_I^k =  -a  \frac{\Delta t}{h_y^2}$

Então:

$\tilde{d}_I^k u_{I-n}^{k+1} 
+ \tilde{b}_I^k u_{I-1}^{k+1} 
+ \tilde{a}_I^k u_{I}^{k+1} 
+ \tilde{c}_I^k u_{I+1}^{k+1} 
+ \tilde{e}_I^k u_{I+n}^{k+1}
= u_I^{k} + \Delta t f_I^{k+1} $

O que nos conduz a equação:

$AU^{k+1} = U^k + \Delta t f^{k+1}$

\newpage
\subsection{Diferença Central}

Reescrevendo usando diferença central:

$\frac{u^{k+1}_I - u^k_I}{\Delta t} 
- a \Big( \frac{u^{k+  1/2}_{I-1} - 2 u^{k+1/2}_I + u^{k+1/2}_{I+1} }{h^2_x} 
+ \frac{u^{k+1/2}_{I-n} - 2 u^{k+1/2}_I + u^{k+1/2}_{I+n}}{h_y^2} \Big) = f^{k+1/2}_I $

Desmembrando os termos:

\begin{center}
$\frac{u^{k+1}_I - u^k_I}{\Delta t} 
- \frac{a}{2} 
\Big[
\frac{u^{k}_{I-n}}{h_y^2}
\frac{u^{k}_{I-1}}{h_x^2} 
- 2 \Big( \frac{1}{h_x^2}  + \frac{1}{h_y^2} \Big) u^{k}_I 
+ \frac{u^{k}_{I+1}}{h^2_x} 
+ \frac{u^{k}_{I+n}}{h^2_y} 
\Big]
- \frac{a}{2} 
\Big[
\frac{u^{k+1}_{I-n}}{h_y^2}
\frac{u^{k+1}_{I-1}}{h_x^2} 
- 2 \Big( \frac{1}{h_x^2}  + \frac{1}{h_y^2} \Big) u^{k+1}_I 
+ \frac{u^{k+1}_{I+1}}{h^2_x} 
+ \frac{u^{k+1}_{I+n}}{h^2_y} 
\Big] $
$
= \frac{1}{2} \Big( f_I^k + f_I^{k+1}\Big)
$
\end{center}

Reordenando:

\begin{center}
$
\Big( -a  \frac{\Delta t}{2 h_y^2} \Big) u_{I-n}^{k+1} 
+ \Big( -a  \frac{\Delta t}{2 h_x^2} \Big) u_{I-1}^{k+1} 
+ \Big[ 1+a\Delta t \Big( \frac{1}{h_x^2} + \frac{1}{h_y^2}  \Big) \Big] u_{I}^{k+1} 
+ \Big( -a  \frac{\Delta t}{2 h_x^2} \Big) u_{I+1}^{k+1} 
+ \Big( -a  \frac{\Delta t}{2 h_y^2} \Big) u_{I+n}^{k+1}$

$ =
\Big( a  \frac{\Delta t}{2 h_y^2} \Big) u_{I-n}^{k} 
+ \Big( a  \frac{\Delta t}{2 h_x^2} \Big) u_{I-1}^{k} 
+ \Big[ 1-a\Delta t \Big( \frac{1}{h_x^2} + \frac{1}{h_y^2}  \Big) \Big] u_{I}^{k} 
+ \Big( a  \frac{\Delta t}{2 h_x^2} \Big) u_{I+1}^{k} 
+ \Big( a  \frac{\Delta t}{2 h_y^2} \Big) u_{I+n}^{k}$

 $+ \frac{\Delta t}{2} \Big( f_I^{k} + f_I^{k+1} \Big) $
\end{center}

Renomeando:

$d_I^{k+1} = e_I^{k+1} = -a  \frac{\Delta t}{2 h_y^2}$

$b_I^{k+1} = c_I^{k+1} = - a  \frac{\Delta t}{2 h_x^2}$

$a_I^{k+1} = 1 +a\Delta t\Big( \frac{1}{h_x^2} + \frac{1}{h_y^2}  \Big)$


$\hat{d}_I^k = \hat{e}_I^k =  a \frac{\Delta t}{2 h_y^2}$

$\hat{b}_I^k = \hat{c}_I^k =  a  \frac{\Delta t}{2 h_x^2}$

$\hat{a}_I^k = 1 -a\Delta t\Big( \frac{1}{h_x^2} + \frac{1}{h_y^2}  \Big)$


Então:

\begin{center}
$ d_I^{k+1} u_{I-n}^{k+1} 
+ b_I^{k+1} u_{I-1}^{k+1} 
+ a_I^{k+1} u_{I}^{k+1} 
+ c_I^{k+1} u_{I+1}^{k+1} 
+ e_I^{k+1} u_{I+n}^{k+1}
= \tilde{d}_I^k u_{I-n}^{k+1} 
+ \tilde{b}_I^k u_{I-1}^{k+1} 
+ \tilde{a}_I^k u_{I}^{k+1} 
+ \tilde{c}_I^k u_{I+1}^{k+1} 
+ \tilde{e}_I^k u_{I+n}^{k+1}
+ \frac{\Delta t}{2} \Big( f_I^{k} + f_I^{k+1} \Big) $
\end{center}

O que nos conduz a equação:

$AU^{k+1} = \hat{A}U^k + \frac{\Delta t}{2} \Big( f^{k} + f^{k+1} \Big)$




\newpage
\chapter{Validação - Equação do Calor com condutividade unitária}

$\frac{\partial u}{\partial t} - \Big( \frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u }{\partial y^2} \Big) = f(x,y,t) $ em $R = (0,1) \times (0,1)$ 

A equação do calor com condutividade unitária é dada acima. Devemos inicialmente calcular o valor de $f(x,y,t)$ para aplicar as técnicas de elementos finitos.

Sabemos que $u_e(x,y) = t^2x(x-1)y(y-1)$
Logo podemos calcular suas derivadas parciais:

$\frac{\partial u}{\partial t} = 2tx(x-1)y(y-1)$

$\frac{\partial u}{\partial x} = t^2(2x-1)y(y-1)$

$\frac{\partial^2 u}{\partial x^2} = 2 t^2 y (y-1)$

$\frac{\partial^2 u}{\partial y^2} = 2 t^2 x (x-1)$

Assim:

$f(x,y,t) = 2tx(x-1)y(y-1) - 2t^2[ x(x-1) + y(y-1) ]$

$f(x,y,t) = 2t[ x(x-1)y(y-1) - tx(x-1) - ty(y-1) ]$

Se substituirmos por $\alpha = x(x-1)$ e $\beta = y(y-1)$ chegaremos a:

$f(x,y,t) = 2t( \alpha \beta - t \alpha - t \beta)$


\newpage
\chapter{---}

% ---
% Conclusão
% ---
\chapter{Conclusão}
% ---
O precondicionamento Seidel se apresentou como uma ótima alternativa na otimização dos métodos GMRES e LCD aplicados as matrizes Rail 1577 e Atmosmodj. Contudo, é possível perceber que ele manteve ou piorou o tempo médio de solução para as matrizes FEM 3D thermal1. A simplicidade da estrutura da matriz de transformação e a presença de matrizes de características distintas nos testes possa, talvez, explicar o comportamento imprevisível do precondicionamento Seildel.



% ----------------------------------------------------------
% Referências bibliográficas
% ----------------------------------------------------------
\chapter*{Bibliografia}


%\bibliography{refs}
%%\bigskip

%%AZEVEDO, M. A.; GUERRA, V. N. A. Mania de bater: a punição corporal doméstica de crianças e adolescentes no Brasil. São Paulo: Iglu, 2001. 386 p.

\noindent \textbf {Catabriga, L.}, Minicurso M16: Estrutura de Dados e Solvers. 2008. Disponível em: <https://inf.ufes.br/~luciac/mn1/precondicionadores.pdf>. Acesso em 24 de junho de 2017.

\noindent \textbf {Ghidetti, K. R.}, O impacto do reordenamento de matrizes esparsas nos métodos iterativos não estacionários precondicionados. 2011. 61p. (Mestrado em Informática) - Programa de Pós-Graduação em Informática, Universidade Federal do Espírito Santo, Espírito Santo

\noindent \textbf {Lugon, B.}, Precondicionadores. Disponível em: <https://inf.ufes.br/~luciac/mn1/ Aula\%20Precondicionadores.pdf>. Acesso em 24 de junho de 2017.


\noindent \textbf {Saad, Y.}, Iterative Methods for Sparse Linear Systems, Society for Industrial and Applied Mathematics, p. 297-315, 2003.




\end{document}
