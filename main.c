/*
 ============================================================================
 Name        : Trab3_AlgNumII.c
 Author      :
 Version     :
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "./CommonFiles/protos.h"
#include "csrMatrixSystem.h"
#include "solvers.h"
#include "vector.h"
#include "metodos.h"



/************************
 * 		   MAIN			*
 ************************/

int main(int argc, char *argv[]) {
	/*if (argc != 2) {
			printf("\n Erro! Sem arquivo de entrada.");
			printf("\n Modo de usar: ./trab3 <arquivo_de entrada> \n");
			return 0;
		}

		// LEITURA
		char* buffer = (char*) calloc(256, sizeof(double));
		FILE* arq = fopen(argv[1], "r");
		if (arq == NULL) {
			printf("Erro: Arquivo nao encontrado para leitura.");
			exit(1);
		}

		fscanf(arq,"%d %d", &m, &n);
		n++; // Para acomodar indices das var basicas
		tableau = alocarTableau(m, n);
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n-1; j++) {
				fscanf(arq,"%lf", &tableau[i][j]);
			}
		}*/

		double m = 5;
		double n = 5;

		const double a = 1;
		double dt = 0.01;
		double tFinal = 1;

		printf("Solucao Exata:\n\n");
		double* u = alocarVetor(m*n);
		solExataEquacaoDoCalor(u,m,n,tFinal);
		printVetorComoMatriz(u,m,n);
		free(u);

		metodoExplicito(a,m,n,dt, tFinal);

		return EXIT_SUCCESS;
}
