#include <stdio.h>
#include "metodos.h"
#include "vector.h"
#include "csrMatrixSystem.h"
#include "solvers.h"

void metodoExplicito(double a, int m, int n, double dt, double tFinal) {

	printf("Resolvendo pelo Metodo Explicito:\nm = %d, n = %d, dt = %lf, tFinal = %lf\n\n", m, n, dt, tFinal);
	int X, Y, alfa, beta;
	double f, t;
	double hx = n-1;
	double hy = m-1;

	double* uAnterior = alocarVetor(m*n);
	double* uNovo = alocarVetor(m*n);
	int nPassos = (int) (tFinal/dt);

	double A, B, C, D, E;
	D = a*dt/(hy*hy);
	B = a*dt/(hx*hx);
	A = 1 - 2*a*dt*((1/hx*hx) + (1/hy*hy));
	C = B;
	E = D;

	// condicoes contorno
	/*for (int i = 0; i < n; i++)
		uAnterior[i] = 0;
	for (int i = m*(n-1); i < m*n; i++)
		uAnterior[i] = 0;
	*/

	for (int k = 0; k <= nPassos; k++) {
		t = k*dt;
		for (int i = n; i < m*(n-1); i++) {

			if (i%n == 0 || i%n == n-1) { // condicoes contorno
				uNovo[i] = 0;
			}
			else {
				uNovo[i] = D*uAnterior[i-n];
				uNovo[i] += B*uAnterior[i-1];
				uNovo[i] += A*uAnterior[i];
				uNovo[i] += C*uAnterior[i+1];
				uNovo[i] += E*uAnterior[i+n];

				X = i%n;
				Y = (int)i/n;
				alfa = X*(X-1);
				beta = Y*(Y-1);
				f = 2*t*(alfa*beta - t*(alfa+beta));
				uNovo[i] += dt*f;
			}
		}
		//printf("t(%d) = %lf\n\n", k, k*dt);
		//printVetorComoMatriz(uNovo, m, n);
		double* temp = uAnterior;
		uAnterior = uNovo;
		uNovo = temp;
	}

	printf("t(%d) = %lf\n\n", nPassos, nPassos*dt);
	printVetorComoMatriz(uAnterior, m, n);

	double erro = calculaErroSolEquacaoDoCalor(uAnterior, m, n, nPassos*dt);
	printf("Erro na solucao = %lf\n", erro);



	freeVetor(uNovo);
	freeVetor(uAnterior);
}


void metodoImplicito(double a, int m, int n, double dt, double tFinal) {

	printf("Resolvendo pelo Metodo Implicito:\nm = %d, n = %d, dt = %lf, tFinal = %lf\n\n", m, n, dt, tFinal);
	int X, Y, alfa, beta;
	double f, t;
	double hx = n-1;
	double hy = m-1;

	double* uAnterior = alocarVetor(m*n);
	double* uNovo = alocarVetor(m*n);
	int nPassos = (int) (tFinal/dt);

	double A, B, C, D, E;
	D = -a*dt/(hy*hy);
	B = D;
	A = 1 + 2*a*dt*((1/hx*hx) + (1/hy*hy));
	C = -a*dt/(hx*hx);
	E = C;

	// Condicao inicial eh matriz nula

	// Constroi Matriz A
	MAT* matrizA = contruirMatrizA(m,n,D,B,A,C,E);

	LinearSystem* ls = (LinearSystem*) malloc (sizeof(LinearSystem));
	ls->A = matrizA;

	for (int k = 0; k <= nPassos; k++) {
		t = k*dt;

		for (int i = n; i < m*(n-1); i++) {

			if (i%n == 0 || i%n == n-1) { // condicoes contorno
				uAnterior[i] = 0;
			}
			else {
				X = i%n;
				Y = (int)i/n;
				alfa = X*(X-1);
				beta = Y*(Y-1);
				f = 2*t*(alfa*beta - t*(alfa+beta));
				uAnterior[i] += dt*f;
			}
		}
		ls->b = uAnterior;
		ls->x = uNovo;
		GMRES_completo(ls, 50, 10000, 0.00000001, NULL, NULL, "", 0, SEM_PRECOND);

		double* temp = uAnterior;
		uAnterior = uNovo;
		uNovo = temp;
	}

	printf("t(%d) = %lf\n\n", nPassos, nPassos*dt);
	printVetorComoMatriz(uAnterior, m, n);

	double erro = calculaErroSolEquacaoDoCalor(uAnterior, m, n, nPassos*dt);
	printf("Erro na solucao = %lf\n", erro);


	freeLinearSystem(ls);
	freeVetor(uNovo);
	freeVetor(uAnterior);
}


void metodoCrankNicolson(double a, int m, int n, double dt, double tFinal) {
	printf("Resolvendo pelo Metodo Implicito:\nm = %d, n = %d, dt = %lf, tFinal = %lf\n\n", m, n, dt, tFinal);
	int X, Y, alfa, beta;
	double f, t;
	double hx = n-1;
	double hy = m-1;

	double* uAnterior = alocarVetor(m*n);
	double* uNovo = alocarVetor(m*n);
	int nPassos = (int) (tFinal/dt);

	double A0, B0, C0, D0, E0;
	D0 = E0 = -a*dt/(2*hy*hy);
	B0 = C0 = -a*dt/(2*hx*hx);
	A0 = 1 + a*dt*((1/hx*hx)+(1/hy*hy));

	double A1, B1, C1, D1, E1;
	D1 = E1 = a*dt/(2*hy*hy);
	B1 = C1 = a*dt/(2*hx*hx);
	A1 = 1 - a*dt*((1/hx*hx)+(1/hy*hy));

	// Constroi Matriz A e B
	MAT* matrizA = contruirMatrizA(m,n,D0,B0,A0,C0,E0);
	MAT* matrizB = contruirMatrizA(m,n,D1,B1,A1,C1,E1);
	//...

	LinearSystem* ls0 = (LinearSystem*) malloc (sizeof(LinearSystem));
	ls0->A = matrizA;

	LinearSystem* ls1 = (LinearSystem*) malloc (sizeof(LinearSystem));
	ls1->A = matrizB;

	// Condicao inicial eh matriz nula

	for (int k = 0; k <= nPassos; k++) {
		t = k*dt;

		produtoMatVet(matrizB, uAnterior, uAnterior);

		for (int i = n; i < m*(n-1); i++) {

			if (i%n == 0 || i%n == n-1) { // condicoes contorno
				uAnterior[i] = 0;
			}
			else {
				X = i%n;
				Y = (int)i/n;
				alfa = X*(X-1);
				beta = Y*(Y-1);
				/* Reescrever
				f = 2*t*(alfa*beta - t*(alfa+beta));
				uAnterior[i] += dt*f;*/
			}
		}

		/* Reescrever
		ls->b = uAnterior;
		ls->x = uNovo;
		GMRES_completo(ls, 50, 10000, 0.00000001, NULL, NULL, "", 0, SEM_PRECOND);*/

		double* temp = uAnterior;
		uAnterior = uNovo;
		uNovo = temp;
	}

	printf("t(%d) = %lf\n\n", nPassos, nPassos*dt);
	printVetorComoMatriz(uAnterior, m, n);

	double erro = calculaErroSolEquacaoDoCalor(uAnterior, m, n, nPassos*dt);
	printf("Erro na solucao = %lf\n", erro);


	freeLinearSystem(ls0);
	freeLinearSystem(ls1);
	freeVetor(uNovo);
	freeVetor(uAnterior);
}

MAT* contruirMatrizA(int m, int n, double D, double B, double A, double C, double E){

	double** tempA = malloc(m*sizeof(double*));
	for (int i = 0; i < m; i++) {
		tempA = calloc(n*sizeof(double));
	}

	for (int i = 0; i < m; i++) {
		/*if (i-n > 0) {
			tempA[i][i-1] = B;
		}

		if (i-1 >= 0) {
			tempA[i][i-1] = B;
		}

		tempA[i][i] = A;

		if (i+1 < n) {
			tempA[i][i-1] = C;
		}*/
	}


	for (int i = 0; i < m; i++) {
		free(tempA);
	}
	free(tempA);

	MAT* matrizA = malloc(sizeof(MAT));
	/*int nz = m*n; // Colocar aqui o numero de elementos
	matrizA->m = m;
	matrizA->n = n;
	matrizA->nz = nz;


	matrizA->AA  = (double *) calloc(nz, sizeof (double));
	matrizA->D   = (double *) calloc(n,  sizeof (double));
	matrizA->JA  = (int    *) calloc(nz, sizeof (int));
	matrizA->IA  = (int    *) calloc(n+1,sizeof (int));

	for (int k = 0; k < nz; k += m) {
			matrizA->AA[k-m] = D;
			matrizA->AA[k-1] = B;
			matrizA->AA[k] = A;
			matrizA->AA[k+1] = C;
			matrizA->AA[k+m] = E;

	}*/
	return matrizA;
}

void printVetorComoMatriz(double* v, int m, int n) {

	for (int x = 0; x < m; x++) {
		for (int y = 0; y < n; y++) {
			printf("%.2lf\t", v[x + y*m]);
		}
		printf("\n");
	}
	printf("\n");
}

void solExataEquacaoDoCalor(double* u, int m, int n, double t) {
	for (int x = 0; x < m; x++) {
		for (int y = 0; y < n; y++) {
			u[x + y*m] =  t*t * x*(x-1) * y*(y-1);
		}
	}

}

double calculaErroSolEquacaoDoCalor(double* u, int m, int n, double t) {
	double solExata, solEncontrada, somaErroNumerador = 0.0, somaErroDenominador = 0.0;
	for (int x = 0; x < m; x++) {
			for (int y = 0; y < n; y++) {
				solExata = t*t * x*(x-1) * y*(y-1);
				solEncontrada = u[x + y*m];
				somaErroNumerador +=  pow(solEncontrada - solExata, 2);
				somaErroDenominador += pow(solExata, 2);
			}
	}

	return sqrt(somaErroNumerador)/sqrt(somaErroDenominador);
}






















